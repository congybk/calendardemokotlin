package com.cam.calendardemo

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.GridLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_month.*
import java.util.*

class MonthFragment : Fragment() {

    companion object {
        private const val KEY_START_CALENDAR = "key_start_calendar"
        private const val NUMBER_DAY_OF_WEEK = 7

        fun newInstance(calendar: Calendar) = MonthFragment().apply {
            val bundle = Bundle()
            bundle.putSerializable(KEY_START_CALENDAR, calendar)
            arguments = bundle
        }
    }

    private lateinit var adapter: MonthAdapter


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_month, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val calendar = arguments?.getSerializable(KEY_START_CALENDAR) as? Calendar
        adapter = MonthAdapter(context!!, calendar!!)
        recyclerView.layoutManager = GridLayoutManager(activity, NUMBER_DAY_OF_WEEK)
        recyclerView.adapter = adapter

    }
}