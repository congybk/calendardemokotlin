package com.cam.calendardemo

import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import java.util.*

class MonthPagerAdapter(fm: FragmentManager, calendar: Calendar?) : FragmentStatePagerAdapter(fm) {

    companion object {
        private const val MAX_MONTH_LIMIT = 12
        private const val MAX_YEAR_LIMIT = 3000
    }

    private val mCalendar: Calendar = calendar ?: Calendar.getInstance()

    override fun getCount(): Int {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.MONTH, MAX_MONTH_LIMIT)
        calendar.set(Calendar.YEAR, MAX_YEAR_LIMIT)
        return TimeUtil.getNumberMonthBetWeenTwoDays(mCalendar, calendar)
    }

    override fun getItem(position: Int): MonthFragment {
        val calendar = Calendar.getInstance()
        calendar.add(
            Calendar.MONTH,
            position - TimeUtil.getNumberMonthBetWeenTwoDays(mCalendar, Calendar.getInstance())
        )
        return MonthFragment.newInstance(calendar)
    }

    override fun getPageTitle(position: Int): CharSequence {
        val calendar = Calendar.getInstance()
        calendar.add(
            Calendar.MONTH,
            position - TimeUtil.getNumberMonthBetWeenTwoDays(mCalendar, Calendar.getInstance())
        )
        return TimeUtil.getFormattedYearMonth(calendar)
    }

}