package com.cam.calendardemo

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import java.util.*

class MonthAdapter(private val context: Context, private val calendar: Calendar) :
    RecyclerView.Adapter<MonthAdapter.ViewHolder>() {

    companion object {
        private const val NUMBER_DAY_OF_WEEK = 7
    }

    override fun onCreateViewHolder(parent: ViewGroup, position: Int): MonthAdapter.ViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.calendar_view_day, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = getSize()

    override fun onBindViewHolder(holder: MonthAdapter.ViewHolder, position: Int) {
        if (isDayOfMonth(position)) {
            val calendarDay = calendar.clone() as Calendar
            calendarDay.set(Calendar.DATE, getDay(position))
            holder.tvDay.text = getDay(position).toString()
        } else {
            holder.tvDay.background = null
        }
    }

    private fun isDayOfMonth(position: Int): Boolean {
        return position < TimeUtil.getDaysYearMonth(calendar) + TimeUtil.getDayPositionOffset(calendar) &&
                position >= TimeUtil.getDayPositionOffset(calendar)
    }

    private fun getDay(position: Int): Int {
        return position - TimeUtil.getDayPositionOffset(calendar) + 1
    }

    private fun getSize(): Int {
        val dayAddOffset = TimeUtil.getDaysYearMonth(calendar) + TimeUtil.getDayPositionOffset(calendar)
        val row = dayAddOffset.toFloat() / NUMBER_DAY_OF_WEEK
        if (row <= 4) {
            return 4 * NUMBER_DAY_OF_WEEK
        }
        return if (row > 4 && row <= 5) {
            5 * NUMBER_DAY_OF_WEEK
        } else 6 * NUMBER_DAY_OF_WEEK
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvDay: TextView = itemView.findViewById(R.id.tvDay)
        internal var llDay: LinearLayout = itemView.findViewById(R.id.llDay)

    }

}